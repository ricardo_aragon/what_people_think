# API Server

## Obiettivo
* API server è il server che si occupa di rendere disponibile la pagina HTML. Riceve in input keyword e nome provider e chiama l'API relativa al provider selezionato passando come parametro la keyword.

## Descrizione
* input : keyword e nome provider provenienti da index.html
* output : file JSON conententi i tweets o reviews.
* run : python ./apiServer/server.py
* Documentazione approfondita [GitHub](https://flask.palletsprojects.com/en/1.1.x/)

## Flusso di esecuzione
Dopo la chiamata del metodo home si può accedere alla pagina principale HTML all'indirizzo
[http://localhost:5000](http://localhost:5000). Le informazioni nome provider e keyword provenienti dalla pagina HTML vengono
inviate attraverso una POST all'indirizzo [http://localhost:5000/get-text](http://localhost:5000/get-text).
Il server successivamente chiama la funzione entryPoint che consente di eseguire lo script della relativa API
(che, una volta finita l'elaborazione, salva il risultato in formato JSON) e, per constatare la correttezza,
una volta che il server ha ricevuto i risultati restituisce il relativo file JSON che sarà visualizzato nella 
pagina HTML.

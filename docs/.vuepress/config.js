module.exports = {
 title: "What do people think?",
 description: "Il progetto si basa sullo sviluppo di un sistema in grado di restituire una distribuzione di probabilità sentiment/aspect di documenti recuperati da diverse fonti, per poter effettuare analisi legate a qualità e soddisfazione sugli articoli analizzati",
 dest: "public",
 base: "/what_people_think/",
 themeConfig: {
   sidebar: ['/' , '/html', '/apiamazon', '/apitwitter' ,'/apiserver', '/asum' ,'/template', '/preprocessing']
 }
}
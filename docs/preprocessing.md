# Preprocessing

## Obiettivo
Preprocessing contiene i test relativi al metodo di preprocessing utilizzato. Si nota che il preprocessing è specifico per la lingua italiana.

## Descrizione
* input : stringa a piacere
* output : parole lemmatizzate, senza stopword
* run : prova_stopwords.py

## Flusso di esecuzione
Durante l'esecuzione si tokenizza la frase e si lemmatizzano le parole rimuovendo le stopword. Si utilizza anche una regex per gestire gli apostrofi. Per la lemmatizzazione si utilizza la libreria 
[spacy](https://spacy.io/api/doc).
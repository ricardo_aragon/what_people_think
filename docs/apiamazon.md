# API Amazon

## Obiettivo
API Amazon permettere la ricerca di recensioni Amazon in base alla keyword inserita

## Descrizione
* input : keyword
* output : Insieme di oggetti Json che rappresentano le recensioni.
* run : python core_extract_comments.py
* Documentazione approfondita [GitHub](https://github.com/philipperemy/amazon-reviews-scraper)

## Flusso di esecuzione
Si realizza la chiamata alla funzione principale get_comments_based_on_keyword che prende in input
la keyword, in seguito si ricupera il soap della pagina web di amazon dall'url e si cerca all'interno
il codice ASIN che serve per trovare il prodotto e le recensioni associate. Infine queste recensioni
vengono resituite in un file Json.
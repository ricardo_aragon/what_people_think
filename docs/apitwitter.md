# API Twitter

## Obiettivo
API Twitter consente la ricerca di tweets in italiano relazionati alla keyword data in input.

## Descrizione
* input : keyword, numero massimo di tweets.
* output : insieme di oggetti Json che rappresentano i tweets.
* run : python scraper_tweets.py
* Documentazione approfondita [GitHub](https://github.com/Mottl/GetOldTweets3.git)

## Flusso di esecuzione
Data la keyword si crea l'oggetto query per effettuare la ricerca, e si fissano i parametri 
di ricerca come paese, lingua e numero massimo. Successivamente si genera la lista che contiene
tutti i tweets, e su questa si effettua la selezione dei parametri di interesse come ID_utente,
ID_content e contet. Infine si restituiscono i tweet in formato Json.
